import React, { PureComponent } from "react";
import { StyleSheet } from "react-native";
import { GameEngine } from "react-native-game-engine";

import Systems from "./systems";
import Entities from "./entities";

export default class BestGameEver extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      running: true,
      score: 0,
    };
    this.gameEngine = null;
  }

  onEvent = (e) => {
    console.log(e);
  };

  render() {
    return (
      <GameEngine
        ref={(ref) => (this.gameEngine = ref)}
        style={styles.container}
        systems={Systems}
        onEvent={this.onEvent}
        entities={Entities()}
      ></GameEngine>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
});
