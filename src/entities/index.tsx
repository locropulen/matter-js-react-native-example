import Circle from "../components/Circle";
import Rectangle from "../components/Rectangle";
import Matter from "matter-js";

import { height, width, heightRatio, widthRatio } from "../utils";

// import Constants from '../utils/constants';

Matter.Common.isElement = () => false; //-- Overriding this function because the original references HTMLElement

export default (restart) => {
  //-- Cleanup existing entities..
  if (restart) {
    Matter.Engine.clear(restart.physics.engine);
  }

  let engine = Matter.Engine.create({ enableSleeping: false });
  let world = engine.world;
  world.gravity.y = 1;

  const entities = {
    physics: { engine: engine, world: world },
    Floor: Rectangle(
      world,
      "red",
      { x: 0, y: height - heightRatio },
      { height: heightRatio * 20, width }
    ),
    Wall: Rectangle(
      world,
      "red",
      { x: -width / 2, y: height - heightRatio },
      { height: height * 2, width: widthRatio * 20 }
    ),
    Wall2: Rectangle(
      world,
      "red",
      { x: width / 2, y: height - heightRatio },
      { height: height * 2, width: widthRatio * 20 }
    ),
    // BallA: Circle(world, "blue", -40, 50, 20, { restitution: 0.7 }),
    // BallC: Circle(world, "blue", -80, 100, 50, { restitution: 0.7 }),
    // BallB: Circle(world, "blue", 0, 70, 30, { restitution: 0.7 }),
  };

  // const randomBetween = (min, max) => {
  //   return Math.ceil(Math.random() * (max - min + 1) + min);
  // };

  // if (Math.random() < 0.5) {
  // }

  const buildBalls = () => {
    for (var i = 0; i < 5; i++) {
      const x = -Math.ceil(Math.random() * -40) + Math.ceil(Math.random() * 40);
      const y = Math.ceil(Math.random() * 40);
      const radius = Math.ceil(Math.random() * 30) + 20;
      const props = {
        restitution: 0.4,
      };
      entities["ball" + i] = Circle(world, "blue", x, y, radius, props);
    }
  };
  buildBalls();
  return {
    ...entities,
  };
};
