import React from "react";
import { View } from "react-native";
import { array, object, string } from "prop-types";
import Matter from "matter-js";

const Circle = (props) => {
  const width = props.radius * 2;
  const height = props.radius * 2;

  const { x, y } = React.useMemo(() => {
    return {
      x: props.body.position.x - width / 2,
      y: props.body.position.y - height / 2,
    };
  }, [props.body.position.x, props.body.position.y]);

  const style = {
    position: "absolute",
    left: x,
    top: y,
    width,
    height,
    borderRadius: props.radius,
    backgroundColor: props.color,
  };
  return <View style={style}></View>;
};

export default (world, color, x, y, radius, ...rest) => {
  const body = Matter.Bodies.circle(x, y, radius, ...rest);
  Matter.World.add(world, [body]);

  return {
    body,
    radius,
    // size: [radius, radius],
    color,
    renderer: <Circle />,
  };
};

Circle.propTypes = {
  size: array,
  body: object,
  color: string,
};
