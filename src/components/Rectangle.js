import React from "react";
import { View, Image } from "react-native";
import { array, object, string } from "prop-types";
import Matter from "matter-js";
// const water = require('../../assets/water.png');

const Rectangle = (props) => {
  const width = props.size[0];
  const height = props.size[1];

  const { x, y } = React.useMemo(() => {
    return {
      x: props.body.position.x - width / 2,
      y: props.body.position.y - height / 2,
    };
  }, [props.body.position.x, props.body.position.y]);

  const style = [
    {
      position: "absolute",
      left: x,
      top: y,
      width,
      height,
      backgroundColor: props.color,
    },
  ];
  return <View style={style}></View>;
};

export default (world, color, pos, size) => {
  const body = Matter.Bodies.rectangle(pos.x, pos.y, size.width, size.height, {
    isStatic: true,
    friction: 1,
  });
  Matter.World.add(world, [body]);

  return {
    body,
    size: [size.width, size.height],
    color,
    renderer: <Rectangle />,
  };
};

Rectangle.propTypes = {
  size: array,
  body: object,
  color: string,
};
